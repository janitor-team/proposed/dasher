Source: dasher
Section: x11
Priority: optional
Maintainer: Debian Accessibility Team <pkg-a11y-devel@alioth-lists.debian.net>
Uploaders: Thibaut Paumard <thibaut@debian.org>, Samuel Thibault <sthibault@debian.org>
Vcs-Git: https://salsa.debian.org/a11y-team/dasher.git
Vcs-Browser: https://salsa.debian.org/a11y-team/dasher
Build-Depends: debhelper-compat (= 13),
               gnome-common,
               gnome-pkg-tools (>= 0.6),
               intltool (>= 0.40.1),
               libglib2.0-dev (>= 2.16.0),
               libcairo2-dev,
               libgtk-3-dev,
               libexpat1-dev,
               libatspi2.0-dev,
               libspeechd-dev,
               yelp-tools,
               docbook-xml
Rules-Requires-Root: no
Standards-Version: 4.6.0
Homepage: http://www.inference.phy.cam.ac.uk/dasher/

Package: dasher
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libatk-adaptor,
         dasher-data (= ${source:Version})
Description: graphical predictive text input system
 Dasher is an information-efficient text-entry interface, driven by natural
 continuous pointing gestures. Dasher is a competitive text-entry system
 wherever a full-size keyboard cannot be used - for example,
 .
  * on a palmtop computer
  * on a wearable computer
  * when operating a computer one-handed, by joystick, touchscreen, trackball,
  or mouse
  * when operating a computer with zero hands (i.e., by head-mouse or by
  eyetracker).
 .
 The eyetracking version of Dasher allows an experienced user to write text
 as fast as normal handwriting - 25 words per minute; using a mouse,
 experienced users can write at 39 words per minute.
 .
 Dasher uses a more advanced prediction algorithm than the T9(tm) system
 often used in mobile phones, making it sensitive to surrounding context.

Package: dasher-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: dasher
Description: Data files for dasher
 Dasher is an information-efficient text-entry interface, driven by natural
 continuous pointing gestures. Dasher is a competitive text-entry system
 wherever a full-size keyboard cannot be used .
 .
 This package contains various arch-independent data files for dasher:
  * alphabet descriptions for all languages
  * letter colours settings
  * training files in all languages
